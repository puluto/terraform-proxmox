
provider "proxmox" {
  version = "~> 0.1.0"
  pm_tls_insecure = true
  pm_api_url = "https://localhost:8006/api2/json"
  pm_user = "root@pam"
  # pm_password = "xxx"
  # use env var, export PM_PASS=password
}

#start vm id
variable "vid" {
  default = 200
}

variable "count" {
  default = 1
}

variable "name" {
  default = "vm-test"
}

#ubuntu,centos,win2008
variable "os" {
  default = "ubuntu"
}

variable "cpu_core" {
  default = 1
}

variable "memory_gb" {
  default = 1
}

variable "disk_gb" {
  default = 10
}

variable "notes" {
  default = "Create with terraform"
}

