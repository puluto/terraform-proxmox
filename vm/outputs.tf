output "ip" {
  value = [
    "${module.vm-ubuntu.ip}",
    "${module.vm-centos.ip}"
  ]
}
output "name" {
  value = [
    "${module.vm-ubuntu.name}",
    "${module.vm-centos.name}"
  ]
}
output "vid" {
  value = [
    "${module.vm-ubuntu.vid}",
    "${module.vm-centos.vid}"
  ]
}
